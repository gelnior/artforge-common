package fr.hd3d.common.client;

public interface ScriptConst
{
    /* Operations */
    public final static String PRE_CREATE = "PRE-CREATE";
    public final static String PRE_UPDATE = "PRE-UPDATE";
    public final static String PRE_DELETE = "PRE-DELETE";
    public final static String CREATE = "CREATE";
    public final static String UPDATE = "UPDATE";
    public final static String DELETE = "DELETE";
}
