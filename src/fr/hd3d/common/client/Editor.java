package fr.hd3d.common.client;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Editor
{
    public static final String DEFAULT = "default";
    public static final String BOOLEAN = "boolean";
    public static final String STRING = "string";
    public static final String LONG = "long";
    public static final String LIST = "list";
    public static final String SHORT_TEXT = "short-text";
    public static final String LONG_TEXT = "long-text";
    public static final String BIG_TEXT = "nig-text";
    public static final String INT = "int";
    public static final String NUMBER_EDITOR = "number";
    public static final String DURATION = "duration";
    public static final String DATE = "date";
    public static final String TASK_TYPE = "task-type";
    public static final String TASK_STATUS = "task-status";
    public static final String RATING = "rating";
    public static final String FLOAT = "float";
    public static final String APPROVAL = "approval";
    public static final String PERSON = "person";
    public static final String PROJECT_STATUS = "project-status";
    public static final String PROJECT_TYPE = "project-type";
    public static final String COLOR = "color";
    public static final String CONTRACT_TYPE = "contract-type";

    public static final String SCREEN_TYPE = "screen-type";
    public static final String SCREEN_QUALIFICATION = "screen-qualification";
    public static final String WORKER_STATUS = "worker-status";
    public static final String LICENSE_TYPE = "license-type";
    public static final String DEVICE_TYPE = "device-type";
    public static final String SOFTWARE = "software";
    public static final String COMPUTER_TYPE = "computer-type";
    public static final String COMPUTER_MODEL = "computer-model";
    public static final String SCREEN_MODEL = "screen-model";
    public static final String INVENTORY_STATUS = "inventory-status";
    public static final String DEVICE_MODEL = "device-model";
    public static final String ROOM = "room";

    /**
     * Key=data type; value=list of editors, the first one is the default. Note: only use string as key for the map,
     * since it is used by gxt.
     */
    public static final Map<String, List<String>> editorMaps = new HashMap<String, List<String>>();

    static
    {
        editorMaps.put(Const.JAVA_LANG_STRING, Arrays.asList(DEFAULT, SHORT_TEXT, LONG_TEXT));

        editorMaps.put(Const.JAVA_LANG_BOOLEAN, Arrays.asList(BOOLEAN));

        editorMaps.put(Entity.APPROVALNOTE, Arrays.asList(APPROVAL));

        editorMaps.put(Const.JAVA_LANG_LONG, Arrays.asList(DURATION, NUMBER_EDITOR));

        editorMaps.put(Const.JAVA_LANG_DOUBLE, Arrays.asList(DURATION, NUMBER_EDITOR));

        editorMaps.put(Const.JAVA_UTIL_DATE, Arrays.asList(DATE));

        editorMaps.put(Const.JAVA_SQL_TIMESTAMP, Arrays.asList(DATE));

        editorMaps.put(Entity.TASKSTATUS, Arrays.asList(TASK_STATUS));

        editorMaps.put(Const.JAVA_LANG_FLOAT, Arrays.asList(FLOAT));

        editorMaps.put(Const.JAVA_LANG_INTEGER, Arrays.asList(INT, RATING, NUMBER_EDITOR));

        editorMaps.put(Const.JAVA_LANG_BYTE, Arrays.asList(INT, RATING, NUMBER_EDITOR));

        editorMaps.put(Entity.CONTRACTTYPE, Arrays.asList(CONTRACT_TYPE));

        editorMaps.put(Const.JAVA_UTIL_LIST, Arrays.asList(LIST));

        editorMaps.put("", Arrays.asList(PROJECT_STATUS));
    }
}
