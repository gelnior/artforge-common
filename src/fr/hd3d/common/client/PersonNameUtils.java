package fr.hd3d.common.client;

/**
 * Utils to format person name.
 * 
 * @author HD3D
 */
public class PersonNameUtils
{
    /**
     * Return the full name of person.
     * 
     * @param lastName
     *            the last name of person
     * @param firstName
     *            the first name of person
     * @param login
     *            the login of person
     * @return the full name
     */
    public static String getFullName(String lastName, String firstName, String login)
    {
        StringBuilder name = new StringBuilder();
        return name.append(lastName).append(" ").append(firstName).append(" - ").append(login).toString();
    }

}
