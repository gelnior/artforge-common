package fr.hd3d.common.client;

public interface Sheet
{
    public static final String ITEMTYPE_ATTRIBUTE = "attribute";
    public static final String ITEMTYPE_METHOD = "method";
    public static final String ITEMTYPE_METADATA = "metaData";
    public static final String ITEMTYPE_COLLECTIONQUERY = "collectionQuery";
    public static final String ITEMTYPE_ITEMSTRANSFORMER = "itemsTransformer";
}
