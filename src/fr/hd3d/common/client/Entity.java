package fr.hd3d.common.client;

public interface Entity
{

    public static final String ACTIVITY = "Activity";
    public static final String SEQUENCE = "Sequence";
    public static final String APPROVALNOTE = "ApprovalNote";
    public static final String PERSON = "Person";
    public static final String CATEGORY = "Category";
    public static final String TASKTYPE = "TaskType";
    public static final String PROJECT = "Project";
    public static final String PROJECTTYPE = "ProjectType";
    public static final String TASKSTATUS = "ETaskStatus";
    public static final String CONTRACTTYPE = "EContractType";
    public static final String PERSONDAY = "PersonDay";
    public static final String SHOT = "Shot";
    public static final String CONSTITUENT = "Constituent";
    public static final String TASK = "Task";
    public static final String SOFTWARE = "Software";
    public static final String DEVICE = "Device";
    public static final String DEVICETYPE = "DeviceType";
    public static final String DEVICEMODEL = "DeviceModel";
    public static final String SCREEN = "Screen";
    public static final String SCREENMODEL = "ScreenModel";
    public static final String LICENSE = "License";
    public static final String POOL = "Pool";
    public static final String COMPUTER = "Computer";
    public static final String LISTVALUES = "ListValues";
}
