package fr.hd3d.common.client;

public interface Constraint
{
    /** URL parameter name to add constraint to request. */
    public static final String CONSTRAINT = "constraint";
    /** URL parameter name to add Lucene constraint to request. */
    public static final String LUCENE_CONSTRAINT = "luceneConstraint";
    /** Constraint operator type key needed for the map form. */
    public static final String TYPE_MAP_FIELD = "type";
    /** Column key needed for the map form. */
    public static final String COLUMN_MAP_FIELD = "column";
    /** Value field key (URL constraint parameter look for operator members in the value field). */
    public static final String VALUE_MAP_FIELD = "value";
    /** Left member key needed for the map form (main member key when there is only one operator). */
    public static final String START_MAP_FIELD = "start";
    /** Right member key needed for the map form. */
    public static final String END_MAP_FIELD = "end";

    public static final String NAME_MAP_FIELD = "name";

    public static final String ORDERBY = "orderBy";
    public static final String ITEM_SORT = "itemSort";

    public static final String PAGINATION = "pagination";
    /** First field key needed for the map form. */
    public static final String FIRST_MAP_FIELD = "first";
    /** Quantity field key needed for the map form. */
    public static final String QUANTITY_MAP_FIELD = "quantity";
    /** Params key for script service. */
    public static final String PARAMS = "params";
}
