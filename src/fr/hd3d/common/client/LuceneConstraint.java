package fr.hd3d.common.client;

public interface LuceneConstraint
{
    public static final String LUCENECONSTRAINT = "luceneConstraint";

    public static final String VALUE = "value";
    public static final String TYPE = "type";
    public static final String COLUMN = "column";
}
