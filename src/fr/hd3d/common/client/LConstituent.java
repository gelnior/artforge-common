package fr.hd3d.common.client;

public interface LConstituent
{

    public static final String CONSTITUENT_HOOK = "hook";
    public static final String CONSTITUENT_CATEGORY = "category";
    public static final String CONSTITUENT_LABEL = "label";
    public static final String CONSTITUENT_DESCRIPTION = "description";
    public static final String CONSTITUENT_DIFFICULTY = "difficulty";
    public static final String CONSTITUENT_TRUST = "trust";
    public static final String CONSTITUENT_DESIGN = "design";
    public static final String CONSTITUENT_MODELING = "modeling";
    public static final String CONSTITUENT_SETUP = "setup";
    public static final String CONSTITUENT_TEXTURING = "texturing";
    public static final String CONSTITUENT_SHADING = "shading";
    public static final String CONSTITUENT_HAIRS = "hairs";
    public static final String CONSTITUENT_PARENTLABEL = "categoryName";
    public static final String CONSTITUENT_TAGS = "tags";
    public static final String CONSTITUENT_CATEGORIESNAMES = "categoriesNames";

    public static final String SIMPLE_CLASS_NAME = "Constituent";
    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LConstituent";

}
