package fr.hd3d.common.client;

public interface ItemConstraint
{
    public static final String ITEMCONSTRAINT = "itemConstraint";
    /** Constraint operator type key needed for the map form. */
    public static final String TYPE_MAP_FIELD = "type";
    /** Column key needed for the map form. */
    public static final String ITEMID_MAP_FIELD = "itemId";
    /** Value field key (URL constraint parameter look for operator members in the value field). */
    public static final String VALUE_MAP_FIELD = "value";
    /** Left member key needed for the map form (main member key when there is only one operator). */
    public static final String START_MAP_FIELD = "start";
    /** Right member key needed for the map form. */
    public static final String END_MAP_FIELD = "end";
    public static final String FIELD_FIELD = "field";

    public static final String ITEMSORT = "itemSort";

}
