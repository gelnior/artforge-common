package fr.hd3d.common.client;

/**
 * @author michael.guiral
 *
 */
public class ScriptMessageGenerator
{

    public static final String ERROR_MESSAGE_PARAMETER = "errorMessage";
    
    public static final String OUTPUT_MESSAGE_PARAMETER = "outputMessage";

    public static final String EXIT_CODE_PARAMETER = "exitCode";

    private static final String ERROR_MESSAGE_JSON_PARAMETER = "\"" + ERROR_MESSAGE_PARAMETER + "\"";
    
    private static final String OUTPUT_MESSAGE_JSON_PARAMETER = "\"" + OUTPUT_MESSAGE_PARAMETER + "\"";

    private static final String EXIT_CODE_JSON_PARAMETER = "\"" + EXIT_CODE_PARAMETER + "\"";

    private int exitCode = -1;

    private String errorMessage = "";
    
    private String outputMessage = "";

    /**
     * @param exitCode the script execution exit code
     * @param errorMessage the script execution error message
     */
    public ScriptMessageGenerator(int exitCode, String errorMessage, String outputMessage)
    {
        this.exitCode = exitCode;
        this.errorMessage = errorMessage;
        this.outputMessage = outputMessage;
    }

    /**
     * @return the script execution exit code
     */
    public int getExitCode()
    {
        return exitCode;
    }

    /**
     * @param exitCode the script execution exit code
     */
    public void setExitCode(int exitCode)
    {
        this.exitCode = exitCode;
    }

    /**
     * @return the script execution error message
     */
    public String getErrorMessage()
    {
        return errorMessage;
    }
    
    public String getOutputMessage()
    {
        return outputMessage;
    }

    /**
     * @param errorMessage the script execution error message
     */
    public void setErrorMessage(String errorMessage)
    {
        this.errorMessage = errorMessage;
    }
    
    public void setOutputMessage(String outputMessage)
    {
        this.outputMessage = outputMessage;
    }

    /**
     * @return the json structure contain error code and error message
     */
    public String errorMessageToJsonString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{").append(EXIT_CODE_JSON_PARAMETER).append(":").append(exitCode).append(",").append(
                ERROR_MESSAGE_JSON_PARAMETER).append(":\"").append(errorMessage).append("\"}");
        return stringBuilder.toString();
    }
    
    public String outputMessageToJsonString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{").append(EXIT_CODE_JSON_PARAMETER).append(":").append(exitCode).append(",").append(
                OUTPUT_MESSAGE_JSON_PARAMETER).append(":\"").append(outputMessage).append("\"}");
        return stringBuilder.toString();
    }

}
