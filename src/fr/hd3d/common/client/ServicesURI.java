package fr.hd3d.common.client;

public interface ServicesURI
{
    public static final String VERSION = "v1/";
    public static final String SERVERTIME = "server-time";
    public static final String MAXRECORD = "max-record";
    public static final String DISPLAY_ASSET_MANAGER = "display-asset";

    // MODULES APPLICATIFS
    public static final String APPLICATIONS = "applications";
    public static final String APPLICATION_ID = "application_name";
    public static final String APPLICATION = APPLICATIONS + "/{" + APPLICATION_ID + '}';

    public static final String MY = "my";
    // lot B
    public static final String UPLOAD = "upload";
    public static final String EXPORT = "export";
    public static final String MY_UPLOAD = MY + '/' + UPLOAD;
    public static final String NEXTREVISION = "next-revision";
    public static final String LAST_FILEREVISIONS = "last-filerevisions";
    public static final String LAST_PROXIES = "last-proxies";
    public static final String REUSEUPLOAD = "reuse-upload";
    public static final String TESTUPLOAD = "test-upload";
    public static final String UNITTESTUPLOAD = "unit-upload";
    public static final String DOWNLOAD = "download";
    public static final String DELETE = "delete";

    public static final String CHECK = "check";

    public static final String INIT_DB = "init-db";
    public static final String INIT_DB_INVENTORY = "init-db-inventory";
    public static final String INIT_DB_WITH_PASTA = "init-db-with-pasta";
    public static final String INIT_DB_WITH_BUNCH_CONSTITUENTS = "init-db-with-bunch-constituents";
    public static final String ADD_DB_REUSE_BANK = "add-db-reuse-bank";

    public static final String INIT_DB_BUNCH_FILES = "init-db-bunch-files";
    public static final String ERASE_DB = "erase-db";
    public static final String CLEANUP = "clean-up";
    public static final String VERSION_BUILD = "version";
    public static final String LUCENE_TEST = "lucene-test";
    public static final String UPDATE_DB = "update-db";
    // A34

    public static final String LOGIN = "login";
    public static final String LOGOUT = "logout";

    public static final String PROJECTS = "projects";
    public static final String PROJECT_ID_ATTRIBUTE = "projectId";
    public static final String PROJECT = PROJECTS + "/{" + PROJECT_ID_ATTRIBUTE + '}';

    public static final String PERSONS = "persons";
    public static final String PERSON_ID_ATTRIBUTE = "personId";
    public static final String PROJECT_PERSONS = PROJECT + '/' + PERSONS;
    public static final String PROJECT_PERSON = PROJECT_PERSONS + "/{" + PERSON_ID_ATTRIBUTE + '}';

    public static final String PERSON = PERSONS + "/{" + PERSON_ID_ATTRIBUTE + '}';
    public static final String PERSON_PROJECTS = PERSON + '/' + PROJECTS;
    public static final String PERSON_PROJECT = PERSON_PROJECTS + "/{" + PROJECT_ID_ATTRIBUTE + '}';

    public static final String LOGIN_ID_ATTRIBUTE = "login";
    public static final String PASSWORD_ID_ATTRIBUTE = "password";

    public static final String TASKS = "tasks";
    public static final String TASK_ID_ATTRIBUTE = "taskId";
    public static final String PROJECT_PERSON_TASKS = PROJECT_PERSON + '/' + TASKS;
    public static final String PROJECT_PERSON_TASK = PROJECT_PERSON_TASKS + "/{" + TASK_ID_ATTRIBUTE + '}';

    public static final String TASK = TASKS + "/{" + TASK_ID_ATTRIBUTE + '}';

    public static final String PERSON_TASKS = PERSON + '/' + TASKS;
    public static final String PERSON_TASK = PERSON_TASKS + "/{" + TASK_ID_ATTRIBUTE + '}';

    public static final String PROJECT_TASKS = PROJECT + '/' + TASKS;
    public static final String PROJECT_TASK = PROJECT_TASKS + "/{" + TASK_ID_ATTRIBUTE + '}';

    public static final String ACTIVITIES = "activities";
    public static final String ACTIVITY_ID_ATTRIBUTE = "activityId";
    public static final String ACTIVITY = ACTIVITIES + "/{" + ACTIVITY_ID_ATTRIBUTE + '}';

    public static final String PROJECT_ACTIVITIES = PROJECT + '/' + ACTIVITIES;
    public static final String PROJECT_ACTIVITY = PROJECT_ACTIVITIES + "/{" + ACTIVITY_ID_ATTRIBUTE + '}';

    public static final String PROJECT_PERSON_ACTIVITIES = PROJECT_PERSON + '/' + ACTIVITIES;
    public static final String PROJECT_PERSON_ACTIVITY = PROJECT_PERSON_ACTIVITIES + "/{" + ACTIVITY_ID_ATTRIBUTE + '}';

    public static final String TASK_ACTIVITIES = "task-activities";
    public static final String TASK_ACTIVITY_ID_ATTRIBUTE = "taskActivityId";
    public static final String TASK_ACTIVITY = TASK_ACTIVITIES + "/{" + TASK_ACTIVITY_ID_ATTRIBUTE + '}';
    public static final String PERSON_ACTIVITIES = PERSON + '/' + ACTIVITIES;
    public static final String MY_TASKACTIVITIES = MY + '/' + TASK_ACTIVITIES;
    public static final String MY_TASKACTIVIY = MY + '/' + TASK_ACTIVITY;
    public static final String PERSON_TASKACTIVITIES = PERSON + '/' + TASK_ACTIVITIES;
    public static final String PERSON_TASKACTIVITY = PERSON_TASKACTIVITIES + "/{" + ACTIVITY_ID_ATTRIBUTE + '}';

    public static final String SIMPLE_ACTIVITIES = "simple-activities";
    public static final String SIMPLE_ACTIVITY_ID_ATTRIBUTE = "simpleActivityId";
    public static final String SIMPLE_ACTIVITY = SIMPLE_ACTIVITIES + "/{" + SIMPLE_ACTIVITY_ID_ATTRIBUTE + '}';
    public static final String PERSON_SIMPLEACTIVITIES = PERSON + '/' + SIMPLE_ACTIVITIES;
    public static final String PERSON_SIMPLEACTIVITY = PERSON_SIMPLEACTIVITIES + "/{" + SIMPLE_ACTIVITY_ID_ATTRIBUTE
            + '}';
    public static final String MY_SIMPLEACTIVITIES = MY + '/' + SIMPLE_ACTIVITIES;
    public static final String MY_SIMPLEACTIVITY = MY + '/' + SIMPLE_ACTIVITY;

    public static final String PERSON_ACTIVITY = PERSON_ACTIVITIES + "/{" + ACTIVITY_ID_ATTRIBUTE + '}';

    public static final String DAYS_AND_ACTIVITIES = "days-and-activities";
    public static final String DAY_ID_ATTRIBUTE = "dayId";

    public static final String PERSON_DAYS_AND_ACTIVITIES = PERSON + '/' + DAYS_AND_ACTIVITIES;
    public static final String PERSON_DAY_AND_ACTIVITIES = PERSON_DAYS_AND_ACTIVITIES + "/{" + DAY_ID_ATTRIBUTE + '}';

    public static final String WORKING_TIMES = "working-times";
    public static final String WORKING_TIME_ID_ATTRIBUTE = "workingTimeId";
    public static final String WORKING_TIME = WORKING_TIMES + "/{" + WORKING_TIME_ID_ATTRIBUTE + '}';

    public static final String DATA = "data";
    public static final String CSV = "csv";

    public static final String TASK_GROUPS = "task-groups";
    public static final String TASK_GROUP_ID_ATTRIBUTE = "taskGroupId";

    public static final String PLANNINGS = "plannings";
    public static final String PLANNING_ID_ATTRIBUTE = "planningId";

    public static final String PROJECT_PLANNINGS = PROJECT + '/' + PLANNINGS;
    public static final String PROJECT_PLANNING = PROJECT_PLANNINGS + "/{" + PLANNING_ID_ATTRIBUTE + '}';

    public static final String PLANNING_TASKGROUPS = PROJECT_PLANNINGS + "/{" + PLANNING_ID_ATTRIBUTE + "}/"
            + TASK_GROUPS;
    public static final String PLANNING_TASKGROUP = PLANNING_TASKGROUPS + "/{" + TASK_GROUP_ID_ATTRIBUTE + '}';

    public static final String ABSENCES = "absences";
    public static final String ABSENCE_ID_ATTRIBUTE = "absenceId";
    public static final String ABSENCE = ABSENCES + "/{" + ABSENCE_ID_ATTRIBUTE + '}';

    public static final String PERSON_ABSENCES = PERSON + '/' + ABSENCES;
    public static final String PERSON_ABSENCE = PERSON_ABSENCES + "/{" + ABSENCE_ID_ATTRIBUTE + '}';

    public static final String MILESTONES = "milestones";
    public static final String MILESTONE_ID_ATTRIBUTE = "milestoneId";
    public static final String MILESTONE = MILESTONES + "/{" + MILESTONE_ID_ATTRIBUTE + '}';

    public static final String EVENTS = "events";
    public static final String EVENT_ID_ATTRIBUTE = "eventId";
    public static final String EVENT = EVENTS + "/{" + EVENT_ID_ATTRIBUTE + '}';

    public static final String DUPLICATE_PLANNINGS = "duplicate-planning-master";
    public static final String PLANNING_MASTER_ID_ATTRIBUTE = "planningMasterId";
    public static final String DUPLICATE_MASTER_PLANNING = PROJECT + '/' + DUPLICATE_PLANNINGS + "/{"
            + PLANNING_MASTER_ID_ATTRIBUTE + '}';
    public static final String SWITCH_MASTER = "switch-master";
    public static final String SWITCH_MASTER_PLANNING = PROJECT_PLANNING + '/' + SWITCH_MASTER + "/{"
            + PLANNING_MASTER_ID_ATTRIBUTE + '}';

    public static final String WORK_OBJECTS_RANGE_CATEGORY = "work-objects-range-category";
    public static final String WORK_OBJECTS_RANGE_TEMPORAL = "work-objects-range-temporal";

    public static final String TASK_CHANGES = "task-changes";
    public static final String TASK_CHANGES_ID_ATTRIBUTE = "taskChangeId";
    public static final String PLANNING_TASKCHANGES = PROJECT_PLANNINGS + "/{" + PLANNING_ID_ATTRIBUTE + "}/"
            + TASK_CHANGES;
    public static final String PLANNING_TASKCHANGE = PLANNING_TASKCHANGES + "/{" + TASK_CHANGES_ID_ATTRIBUTE + '}';

    public static final String FILTERED_TASKGROUP_TASKS = "filtered-task-group-tasks";
    public static final String PROJECT_FILTERED_TASKGROUP_TASKS = PROJECT_PLANNING + '/' + FILTERED_TASKGROUP_TASKS;

    public static final String EXTRA_LINES = "extra-lines";
    public static final String EXTRA_LINES_ID_ATTRIBUTE = "extraLineID";

    public static final String TASKGROUP_EXTRALINES = PLANNING_TASKGROUP + '/' + EXTRA_LINES;
    public static final String TASKGROUP_EXTRALINE = PLANNING_TASKGROUP + '/' + EXTRA_LINES + "/{"
            + EXTRA_LINES_ID_ATTRIBUTE + '}';

    public static final String PLANNING_PLANNED_TASKS = "planned-tasks";
    public static final String PROJECT_PLANNING_PLANNED_TASKS = PROJECT_PLANNING + '/' + PLANNING_PLANNED_TASKS;

    public static final String PLANNING_EXPORT_ODS = "planning-export-ods";
    public static final String EXPORT_ODS = "export-ods";
    public static final String EXPORT_STAT_ODS = "export-stat-ods";
    public static final String EXPORT_TASK_ODS = "export-task-ods";
    public static final String PROJECT_PLANNING_EXPORT_ODS = PROJECT + '/' + PLANNINGS + "/{" + PLANNING_ID_ATTRIBUTE
            + "}/" + PLANNING_EXPORT_ODS;

    public static final String CATEGORIES = "categories";
    public static final String CATEGORY_ID_ATTRIBUTE = "categoryId";

    public static final String PROJECT_CATEGORIES = PROJECT + '/' + CATEGORIES;
    public static final String PROJECT_CATEGORY = PROJECT_CATEGORIES + "/{" + CATEGORY_ID_ATTRIBUTE + '}';
    // A12
    public static final String CONSTITUENTS = "constituents";
    public static final String CONSTITUENT_ID_ATTRIBUTE = "constituentId";
    public static final String CONSTITUENT = CONSTITUENTS + "/{" + CONSTITUENT_ID_ATTRIBUTE + '}';

    public static final String CATEGORY_CONSTITUENTS = PROJECT_CATEGORY + '/' + CONSTITUENTS;
    public static final String CATEGORY_CONSTITUENT = CATEGORY_CONSTITUENTS + "/{" + CONSTITUENT_ID_ATTRIBUTE + '}';
    public static final String CATEGORY_CONSTITUENT_TASKS = CATEGORY_CONSTITUENT + "/" + TASKS;
    public static final String PROJECT_CONSTITUENTS = PROJECT + '/' + CONSTITUENTS;
    public static final String PROJECT_CONSTITUENT = PROJECT_CONSTITUENTS + "/{" + CONSTITUENT_ID_ATTRIBUTE + '}';;

    public static final String CONSTITUENTLINKS = "constituent-links";
    public static final String CONSTITUENTLINK_ID_ATTRIBUTE = "constituentlinkId";
    public static final String CONSTITUENTLINK = CONSTITUENTLINKS + "/{" + CONSTITUENTLINK_ID_ATTRIBUTE + '}';

    public static final String COMPOSITIONS = "compositions";
    public static final String COMPOSITION_ID_ATTRIBUTE = "compositionId";
    public static final String COMPOSITION = COMPOSITIONS + "/{" + COMPOSITION_ID_ATTRIBUTE + '}';
    public static final String CONSTITUENT_COMPOSITIONS = CATEGORY_CONSTITUENT + '/' + COMPOSITIONS;
    public static final String CONSTITUENT_COMPOSITION = CONSTITUENT_COMPOSITIONS + "/{" + COMPOSITION_ID_ATTRIBUTE
            + '}';
    public static final String CONSTITUENT_TASKS = PROJECT_CONSTITUENT + '/' + TASKS;

    public static final String ALL = "all";
    public static final String CHILDREN = "children";
    public static final String ALLCHILDREN = "all-children";
    public static final String ALL_PROJECT_CATEGORIES = PROJECT_CATEGORIES + '/' + ALL;

    public static final String ASSETS = "assets";

    public static final String CONSTITUENT_ASSETS = CATEGORY_CONSTITUENT + '/' + ASSETS;

    public static final String CATEGORY_CHILDREN = PROJECT_CATEGORIES + "/{" + CATEGORY_ID_ATTRIBUTE + "}/" + CHILDREN;

    public static final String PLANNING_WORKOBJECTS_RANGE_CATEGORY = PROJECT + '/' + PLANNINGS + "/{"
            + PLANNING_ID_ATTRIBUTE + "}/" + WORK_OBJECTS_RANGE_CATEGORY + "/{" + CATEGORY_ID_ATTRIBUTE + '}';

    public static final String SEQUENCES = "sequences";
    public static final String SEQUENCE_ID_ATTRIBUTE = "sequenceId";

    public static final String SHOTS = "shots";
    public static final String SHOT_ID_ATTRIBUTE = "shotId";
    public static final String SHOT = SHOTS + "/{" + SHOT_ID_ATTRIBUTE + '}';
    public static final String PROJECT_SHOTS = PROJECT + '/' + SHOTS;
    public static final String PROJECT_SHOT = PROJECT + '/' + SHOTS + "/{" + SHOT_ID_ATTRIBUTE + '}';
    public static final String CONSTITUENT_SHOTS = PROJECT_CONSTITUENT + '/' + SHOTS;
    public static final String CONSTITUENT_SHOTS_TASKS = CATEGORY_CONSTITUENT + '/' + SHOTS + '/' + TASKS + "";

    public static final String PROJECT_SEQUENCES = PROJECT + '/' + SEQUENCES;
    public static final String PROJECT_SEQUENCE = PROJECT_SEQUENCES + "/{" + SEQUENCE_ID_ATTRIBUTE + '}';
    public static final String SEQUENCE_SHOTS = PROJECT_SEQUENCE + '/' + SHOTS;
    public static final String SEQUENCE_SHOT = SEQUENCE_SHOTS + "/{" + SHOT_ID_ATTRIBUTE + '}';
    public static final String SEQUENCE_SHOT_TASKS = SEQUENCE_SHOT + "/" + TASKS;
    public static final String SEQUENCE_SHOT_COMPOSITIONS = SEQUENCE_SHOT + '/' + COMPOSITIONS;
    public static final String PROJECT_SHOT_CONSTITUENTS = PROJECT_SHOT + '/' + CONSTITUENTS;
    public static final String SHOT_TASKS = PROJECT_SHOT + '/' + TASKS;
    public static final String PROJECT_SHOT_CONSTITUENTS_TASKS = SEQUENCE_SHOT + '/' + CONSTITUENTS + '/' + TASKS + "";

    public static final String SHOT_ASSETS = SEQUENCE_SHOT + '/' + ASSETS;
    public static final String SEQUENCE_COMPOSITIONS = PROJECT_SEQUENCE + '/' + COMPOSITIONS;
    public static final String PROJECT_SEQUENCES_ALL = PROJECT_SEQUENCES + '/' + ALL;
    public static final String PROJECT_SEQUENCES_ALL_ANN = SEQUENCES + '/' + "annotations";
    public static final String PROJECT_SEQUENCE_CHILDREN = PROJECT_SEQUENCES + "/{" + SEQUENCE_ID_ATTRIBUTE + "}/"
            + CHILDREN;

    public static final String ALL_SEQUENCES = PROJECT_SEQUENCES + '/' + CHILDREN;
    public static final String SEQUENCE_ALL_CHILDREN = PROJECT_SEQUENCES + "/{" + SEQUENCE_ID_ATTRIBUTE + "}/"
            + ALLCHILDREN;

    public static final String PLANNING_WORKOBJECTS_RANGE_SEQUENCE = PROJECT + '/' + PLANNINGS + "/{"
            + PLANNING_ID_ATTRIBUTE + "}/" + WORK_OBJECTS_RANGE_TEMPORAL + "/{" + SEQUENCE_ID_ATTRIBUTE + '}';

    public static final String SHEETS = "sheets";
    public static final String SHEET_ID_ATTRIBUTE = "sheetId";
    public static final String SHEET = SHEETS + "/{" + SHEET_ID_ATTRIBUTE + '}';
    public static final String SHEET_DATA = SHEET + '/' + DATA;
    public static final String SHEET_DATA_CSV = SHEET_DATA + '/' + CSV;
    public static final String PROJECT_SHEETS = PROJECT + '/' + SHEETS;
    public static final String PROJECT_SHEET = PROJECT_SHEETS + "/{" + SHEET_ID_ATTRIBUTE + '}';
    public static final String PROJECT_SHEET_DATA = PROJECT_SHEET + '/' + DATA;
    public static final String PROJECT_SHEET_DATA_CSV = PROJECT_SHEET_DATA + '/' + CSV;

    public static final String ITEMGROUPS = "item-groups";
    public static final String ITEMGROUP_ID_ATTRIBUTE = "itemgroupId";
    public static final String SHEET_ITEMGROUPS = SHEET + '/' + ITEMGROUPS;
    public static final String SHEET_ITEMGROUP = SHEET_ITEMGROUPS + "/{" + ITEMGROUP_ID_ATTRIBUTE + '}';
    public static final String ITEMS = "items";
    public static final String ITEM_ID_ATTRIBUTE = "itemId";
    public static final String SHEET_ITEMS = SHEET + '/' + ITEMS;
    public static final String SHEET_ITEMGROUP_ITEMS = SHEET_ITEMGROUP + '/' + ITEMS;
    public static final String SHEET_ITEMGROUP_ITEM = SHEET_ITEMGROUP_ITEMS + "/{" + ITEM_ID_ATTRIBUTE + '}';
    public static final String PROJECT_SHEET_ITEMGROUPS = PROJECT_SHEETS + "/{" + SHEET_ID_ATTRIBUTE + "}/"
            + ITEMGROUPS;
    public static final String PROJECT_SHEET_ITEMGROUP = PROJECT_SHEET_ITEMGROUPS + "/{" + ITEMGROUP_ID_ATTRIBUTE + '}';
    public static final String PROJECT_SHEET_ITEMGROUP_ITEMS = PROJECT_SHEET_ITEMGROUP + '/' + ITEMS;
    public static final String PROJECT_SHEET_ITEMGROUP_ITEM = PROJECT_SHEET_ITEMGROUP_ITEMS + "/{" + ITEM_ID_ATTRIBUTE
            + '}';

    public static final String CASTING = "casting";
    public static final String CASTING_EXPORT = "casting-export";
    public static final String EXPORT_CASTING = PROJECT + '/' + CASTING_EXPORT;
    public static final String SHOT_CASTING = SEQUENCE_SHOT + '/' + CASTING;
    public static final String SEQUENCE_CASTING = PROJECT_SEQUENCE + '/' + CASTING;

    public static final String ROOTS = "roots";

    public static final String TAGS = "tags";
    public static final String TAG_ID_ATTRIBUTE = "tagId";
    public static final String TAG = TAGS + "/{" + TAG_ID_ATTRIBUTE + '}';

    public static final String TAG_CATEGORIES = "tag-categories";
    public static final String TAGCATEGORY_ID_ATTRIBUTE = "tagcategoryId";
    public static final String TAG_CATEGORIES_ALL_CHILDREN = TAG_CATEGORIES + '/' + ALLCHILDREN;
    public static final String TAG_CATEGORY = TAG_CATEGORIES + "/{" + TAGCATEGORY_ID_ATTRIBUTE + '}';
    public static final String TAG_CATEGORY_CHILDREN = TAG_CATEGORIES + "/{" + TAGCATEGORY_ID_ATTRIBUTE + "}/"
            + CHILDREN;
    public static final String TAG_CATEGORY_TAGS = TAG_CATEGORIES + "/{" + TAGCATEGORY_ID_ATTRIBUTE + "}/" + TAGS;

    public static final String SEARCH_CONSTITUENTS = "search-constituents";
    public static final String SEARCH_FILES = "search-files";

    public static final String CLASSDYNMETADATATYPES = "class-dyn-metadata-types";
    public static final String CLASSDYNMETADATATYPE_ID_ATTRIBUTE = "classdynmetadatatypeId";
    public static final String CLASSDYNMETADATATYPE = CLASSDYNMETADATATYPES + "/{" + CLASSDYNMETADATATYPE_ID_ATTRIBUTE
            + '}';
    public static final String CLASSDYNMETADATATYPE_CHECK = CLASSDYNMETADATATYPE + '/' + CHECK;

    public static final String DYNMETADATATYPES = "dyn-metadata-types";
    public static final String DYNMETADATATYPE_ID_ATTRIBUTE = "dynmetadatatypeId";
    public static final String DYNMETADATATYPE = DYNMETADATATYPES + "/{" + DYNMETADATATYPE_ID_ATTRIBUTE + '}';

    public static final String DYNMETADATAVALUES = "dyn-metadata-values";
    public static final String DYNMETADATAVALUE_ID_ATTRIBUTE = "dynmetadatavalueId";
    public static final String DYNMETADATAVALUE = DYNMETADATAVALUES + "/{" + DYNMETADATAVALUE_ID_ATTRIBUTE + '}';

    public static final String SCRIPTS = "scripts";
    public static final String SCRIPT_ID_ATTRIBUTE = "scriptId";
    public static final String SCRIPT = SCRIPTS + "/{" + SCRIPT_ID_ATTRIBUTE + '}';

    public static final String AUDITS = "audits";

    public static final String SELF = "self";
    public static final String SETTINGS = "settings";
    public static final String SETTING_ID_ATTRIBUTE = "settingId";
    public static final String SETTING = SETTINGS + "/{" + SETTING_ID_ATTRIBUTE + '}';
    public static final String SELF_SETTING = SETTINGS + '/' + SELF;

    public static final String SHEET_FILTERS = "sheet-filters";
    public static final String SHEET_FILTER_ID_ATTRIBUTE = "sheetFilterId";
    public static final String SHEET_FILTER = SHEET_FILTERS + "/{" + SHEET_FILTER_ID_ATTRIBUTE + '}';

    public static final String FILTERS = "filters";
    public static final String SHEET_FILTERS_BY_SHEET = SHEET + '/' + FILTERS;

    public static final String UPDATESSTACK = "updates-stack";

    public static final String ASSETREVISIONS = "asset-revisions";
    public static final String MY_ASSETREVISIONS = MY + '/' + ASSETREVISIONS;
    public static final String ASSETREVISION_ID_ATTRIBUTE = "assetRevisionId";
    public static final String ASSETREVISION = ASSETREVISIONS + "/{" + ASSETREVISION_ID_ATTRIBUTE + '}';

    public static final String ASSETREVISIONLINKS = "asset-revision-links";
    public static final String ASSETREVISIONLINK_ID_ATTRIBUTE = "assetrevisionlinkId";
    public static final String ASSETREVISIONLINK = ASSETREVISIONLINKS + "/{" + ASSETREVISIONLINK_ID_ATTRIBUTE + '}';

    public static final String ASSETREVISIONGROUPS = "asset-revision-groups";
    public static final String ASSETREVISIONGROUP_ID_ATTRIBUTE = "assetrevisiongroupId";
    public static final String ASSETREVISIONGROUP = ASSETREVISIONGROUPS + "/{" + ASSETREVISIONGROUP_ID_ATTRIBUTE + '}';

    public static final String PROJECT_ASSETREVISIONGROUPS = PROJECT + '/' + ASSETREVISIONGROUPS;
    public static final String PROJECT_ASSETREVISIONGROUP = PROJECT + '/' + ASSETREVISIONGROUP;

    public static final String ASSETABSTRACTS = "assets";
    public static final String ASSETABSTRACT_ID_ATTRIBUTE = "assetHook";
    public static final String ASSETABSTRACT = ASSETABSTRACTS + "/{" + ASSETABSTRACT_ID_ATTRIBUTE + '}';
    public static final String ASSETABSTRACT_REVISIONS = ASSETABSTRACT + '/' + ASSETREVISIONS;
    public static final String ASSETABSTRACT_REVISION = ASSETABSTRACT + '/' + ASSETREVISION;
    public static final String PROJECT_ASSETABSTRACTS = PROJECT + '/' + ASSETABSTRACTS;
    public static final String PROJECT_ASSETABSTRACT = PROJECT + '/' + ASSETABSTRACT;
    public static final String PROJECT_ASSETABSTRACT_ASSETREVISIONS = PROJECT + '/' + ASSETABSTRACT + '/'
            + ASSETREVISIONS;
    public static final String PROJECT_ASSETABSTRACT_ASSETREVISION = PROJECT + '/' + ASSETABSTRACT + '/'
            + ASSETREVISION;

    public static final String ASSETREVISIONGROUP_ASSETABSTRACTS = ASSETREVISIONGROUP + '/' + ASSETABSTRACTS;
    public static final String ASSETREVISIONGROUP_ASSETABSTRACT = ASSETREVISIONGROUP + '/' + ASSETABSTRACT;
    public static final String ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISIONS = ASSETREVISIONGROUP + '/'
            + ASSETABSTRACT + '/' + ASSETREVISIONS;
    public static final String ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION = ASSETREVISIONGROUP + '/'
            + ASSETABSTRACT + '/' + ASSETREVISION;
    public static final String PROJECT_ASSETREVISIONGROUP_ASSETABSTRACTS = PROJECT + '/'
            + ASSETREVISIONGROUP_ASSETABSTRACTS;

    public static final String PROJECT_ASSETREVISIONGROUP_ASSETABSTRACT = PROJECT + '/'
            + ASSETREVISIONGROUP_ASSETABSTRACT;

    public static final String PROJECT_ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISIONS = PROJECT + '/'
            + ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISIONS;

    public static final String PROJECT_ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION = PROJECT + '/'
            + ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION;

    public static final String UPSTREAM = "up-stream";
    public static final String DOWNSTREAM = "down-stream";
    public static final String ASSETREVISION_UPSTREAM = ASSETREVISION + '/' + UPSTREAM;
    public static final String ASSETREVISION_DOWNSTREAM = ASSETREVISION + '/' + DOWNSTREAM;
    public static final String PROJECT_ASSETABSTRACT_ASSETREVISION_UPSTREAM = PROJECT_ASSETABSTRACT_ASSETREVISION + '/'
            + UPSTREAM;
    public static final String PROJECT_ASSETABSTRACT_ASSETREVISION_DOWNSTREAM = PROJECT_ASSETABSTRACT_ASSETREVISION
            + '/' + DOWNSTREAM;
    public static final String ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION_UPSTREAM = ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION
            + '/' + UPSTREAM;
    public static final String ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION_DOWNSTREAM = ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION
            + '/' + DOWNSTREAM;
    public static final String PROJECT_ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION_UPSTREAM = PROJECT + '/'
            + ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION_UPSTREAM;
    public static final String PROJECT_ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION_DOWNSTREAM = PROJECT + '/'
            + ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION_DOWNSTREAM;

    public static final String PLAYLISTREVISIONS = "playlist-revisions";
    public static final String PLAYLISTREVISION_ID_ATTRIBUTE = "playListRevisionId";
    public static final String PLAYLISTREVISION = PLAYLISTREVISIONS + "/{" + PLAYLISTREVISION_ID_ATTRIBUTE + '}';

    public static final String PLAYLISTREVISIONGROUPS = "playlist-revision-groups";
    public static final String PLAYLISTREVISIONGROUP_ID_ATTRIBUTE = "playlistrevisiongroupId";

    public static final String PLAYLISTREVISIONGROUP = PLAYLISTREVISIONGROUPS + "/{"
            + PLAYLISTREVISIONGROUP_ID_ATTRIBUTE + '}';
    public static final String PROJECT_PLAYLISTREVISIONGROUPS = PROJECT + '/' + PLAYLISTREVISIONGROUPS;
    public static final String PROJECT_PLAYLISTREVISIONGROUP = PROJECT + '/' + PLAYLISTREVISIONGROUP;
    public static final String PROJECT_PLAYLISTREVISIONGROUP_CHILDREN = PROJECT + '/' + PLAYLISTREVISIONGROUPS + "/{"
            + PLAYLISTREVISIONGROUP_ID_ATTRIBUTE + '}' + '/' + CHILDREN;
    public static final String PROJECT_PLAYLISTREVISIONGROUP_PLAYLISTREVISIONS = PROJECT + '/' + PLAYLISTREVISIONGROUPS
            + "/{" + PLAYLISTREVISIONGROUP_ID_ATTRIBUTE + '}' + '/' + PLAYLISTREVISIONS;

    public static final String PLAYLISTEDITS = "playlist-edits";
    public static final String PLAYLISTEDIT_ID_ATTRIBUTE = "playListEditId";
    public static final String PLAYLISTEDIT = PLAYLISTEDITS + "/{" + PLAYLISTEDIT_ID_ATTRIBUTE + '}';

    public static final String FILEREVISION_ANNOTATIONS = "filerevision-annotations";
    public static final String FILEREVISION_ANNOTATION_ID_ATTRIBUTE = "filerevisionAnnotationId";
    public static final String FILEREVISION_ANNOTATION = FILEREVISION_ANNOTATIONS + "/{"
            + FILEREVISION_ANNOTATION_ID_ATTRIBUTE + '}';

    public static final String GRAPHICAL_ANNOTATIONS = "graphical-annotations";
    public static final String GRAPHICAL_ANNOTATION_ID_ATTRIBUTE = "graphicalAnnotationId";
    public static final String GRAPHICAL_ANNOTATION = GRAPHICAL_ANNOTATIONS + "/{" + GRAPHICAL_ANNOTATION_ID_ATTRIBUTE
            + '}';

    public static final String APPROVALNOTES = "approval-notes";
    public static final String APPROVALNOTE_ID_ATTRIBUTE = "approval";
    public static final String APPROVALNOTE = APPROVALNOTES + "/{" + APPROVALNOTE_ID_ATTRIBUTE + '}';

    public static final String APPROVALNOTETYPES = "approval-note-types";
    public static final String APPROVALNOTETYPE_ID_ATTRIBUTE = "approvaltype";
    public static final String APPROVALNOTETYPE = APPROVALNOTETYPES + "/{" + APPROVALNOTETYPE_ID_ATTRIBUTE + '}';
    public static final String PROJECT_APPROVALNOTETYPES = PROJECT + '/' + APPROVALNOTETYPES;
    public static final String PROJECT_APPROVALNOTETYPE = PROJECT_APPROVALNOTETYPES + "/{"
            + APPROVALNOTETYPE_ID_ATTRIBUTE + '}';

    public static final String CONSTITUENT_NOTES = PROJECT_CONSTITUENT + '/' + APPROVALNOTES;
    public static final String CATEGORY_CONSTITUENT_NOTES = CATEGORY_CONSTITUENT + '/' + APPROVALNOTES;
    public static final String SHOT_NOTES = PROJECT_SHOT + '/' + APPROVALNOTES;
    public static final String SEQUENCE_SHOT_NOTES = SEQUENCE_SHOT + '/' + APPROVALNOTES;

    public static final String ENTITYTASKLINKS = "entity-task-links";
    public static final String ENTITYTASKLINK_ID_ATTRIBUTE = "entitytasklink";
    public static final String ENTITYTASKLINK = ENTITYTASKLINKS + "/{" + ENTITYTASKLINK_ID_ATTRIBUTE + '}';

    public static final String TASK_TYPES = "task-types";
    public static final String TASK_TYPE_ID_ATTRIBUTE = "taskTypeId";
    public static final String TASKTYPE = TASK_TYPES + "/{" + TASK_TYPE_ID_ATTRIBUTE + '}';

    public static final String STEPS = "steps";
    public static final String STEP_ID_ATTRIBUTE = "stepId";
    public static final String STEP = STEPS + "/{" + STEP_ID_ATTRIBUTE + '}';
    public static final String PROJECT_STEPS = PROJECTS + "/{" + PROJECT_ID_ATTRIBUTE + "}/" + STEPS;

    public static final String PROJECT_TYPES = "project-types";
    public static final String PROJECT_TYPE_ID_ATTRIBUTE = "projectTypeId";
    public static final String PROJECT_TYPE = PROJECT_TYPES + "/{" + PROJECT_TYPE_ID_ATTRIBUTE + '}';

    public static final String FILESYSTEM = "filesystem";
    public static final String FILESYSTEM_ID_ATTRIBUTE = "filesystemId";
    public static final String FILESYSTEM_BY_ID = FILESYSTEM + "/{" + FILESYSTEM_ID_ATTRIBUTE + '}';

    public static final String FILEREVISIONS = "file-revisions";
    public static final String MY_FILEREVISIONS = MY + '/' + FILEREVISIONS;
    public static final String FILEREVISION_ID_ATTRIBUTE = "filerevisionId";
    public static final String FILEREVISION = FILEREVISIONS + "/{" + FILEREVISION_ID_ATTRIBUTE + '}';
    public static final String FILEREVISION_DOWNLOAD = FILEREVISION + '/' + DOWNLOAD;
    public static final String FILEREVISION_DOWNSTREAM = FILEREVISION + '/' + DOWNSTREAM;
    public static final String FILEREVISION_UPSTREAM = FILEREVISION + '/' + UPSTREAM;

    public static final String ASSETREVISION_FILEREVISIONS = ASSETREVISION + '/' + FILEREVISIONS;
    public static final String ASSETREVISION_FILEREVISION = ASSETREVISION + '/' + FILEREVISION;
    public static final String ASSETREVISION_LAST_FILEREVISION = ASSETREVISION + '/' + LAST_FILEREVISIONS;
    public static final String ASSETABSTRACT_REVISION_LAST_FILEREVISIONS = ASSETABSTRACT + '/' + ASSETREVISION + '/'
            + LAST_FILEREVISIONS;
    public static final String ASSETABSTRACT_REVISION_FILEREVISION = ASSETABSTRACT + '/' + ASSETREVISION + '/'
            + FILEREVISION;
    public static final String ASSETABSTRACT_REVISION_UPSTREAM = ASSETABSTRACT + '/' + ASSETREVISION + '/' + UPSTREAM;
    public static final String ASSETABSTRACT_REVISION_DOWNSTREAM = ASSETABSTRACT + '/' + ASSETREVISION + '/'
            + DOWNSTREAM;
    public static final String PROJECT_ASSETABSTRACT_ASSETREVISION_FILEREVISIONS = PROJECT + '/' + ASSETABSTRACT + '/'
            + ASSETREVISION + '/' + FILEREVISIONS;
    public static final String PROJECT_ASSETABSTRACT_ASSETREVISION_FILEREVISION = PROJECT + '/' + ASSETABSTRACT + '/'
            + ASSETREVISION + '/' + FILEREVISION;
    public static final String ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION_FILEREVISIONS = ASSETREVISIONGROUP + '/'
            + ASSETABSTRACT + '/' + ASSETREVISION + '/' + FILEREVISIONS;
    public static final String ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION_FILEREVISION = ASSETREVISIONGROUP + '/'
            + ASSETABSTRACT + '/' + ASSETREVISION + '/' + FILEREVISION;
    public static final String PROJECT_ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION_FILEREVISIONS = PROJECT + '/'
            + ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION_FILEREVISIONS;
    public static final String PROJECT_ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION_FILEREVISION = PROJECT + '/'
            + ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION_FILEREVISION;

    // public static final String ASSETREVISIONFILEREVISIONS = "asset-revision-file-revisions";
    // public static final String ASSETREVISIONFILEREVISION_ID_ATTRIBUTE = "assetrevisionfilerevisionId";
    // public static final String ASSETREVISIONFILEREVISION = ASSETREVISIONFILEREVISIONS + "/{"
    // + ASSETREVISIONFILEREVISION_ID_ATTRIBUTE + '}';

    public static final String FILEREVISIONLINKS = "file-revision-links";
    public static final String FILEREVISIONLINK_ID_ATTRIBUTE = "filerevisionlinkId";
    public static final String FILEREVISIONLINK = FILEREVISIONLINKS + "/{" + FILEREVISIONLINK_ID_ATTRIBUTE + '}';

    public static final String INPUT_FILEREVISION_ID_ATTRIBUTE = "inputfilerevisionId";
    public static final String OUTPUT_FILEREVISION_ID_ATTRIBUTE = "outputfilerevisionId";
    public static final String INPUT = "input";
    public static final String OUTPUT = "output";
    public static final String DELETE_FILEREVISION_BY_INPUT_OUTPUT = FILEREVISIONLINKS + '/' + INPUT + "/{"
            + INPUT_FILEREVISION_ID_ATTRIBUTE + "}/" + OUTPUT + "/{" + OUTPUT_FILEREVISION_ID_ATTRIBUTE + '}';

    public static final String FILE_ATTRIBUTES = "file-attributes";
    public static final String FILE_ATTRIBUTE_ID_ATTRIBUTE = "fileattributeId";
    public static final String FILE_ATTRIBUTE = FILE_ATTRIBUTES + "/{" + FILE_ATTRIBUTE_ID_ATTRIBUTE + '}';

    public static final String NEXT_FILEREVISION_UPLOAD = FILEREVISIONS + '/' + NEXTREVISION + "/{"
            + FILEREVISION_ID_ATTRIBUTE + "}/" + UPLOAD;
    public static final String NEXT_FILEREVISION = FILEREVISIONS + '/' + NEXTREVISION + "/{"
            + FILEREVISION_ID_ATTRIBUTE + "}";

    // lot C
    public static final String ICES = "ices";
    public static final String ICESDOWNLOAD = "ices-download";
    public static final String TESTICES = "test-ices";

    // Resources
    public static final String RESOURCEGROUPS = "resource-groups";
    public static final String RESOURCEGROUP_ID_ATTRIBUTE = "resourcegroupId";
    public static final String RESOURCEGROUP = RESOURCEGROUPS + "/{" + RESOURCEGROUP_ID_ATTRIBUTE + '}';
    public static final String RESOURCEGROUP_PERSONS = RESOURCEGROUP + '/' + PERSONS;
    public static final String RESOURCEGROUP_PERSON = RESOURCEGROUP_PERSONS + "/{" + PERSON_ID_ATTRIBUTE + '}';
    public static final String PROJECT_RESOURCEGROUPS = PROJECT + '/' + RESOURCEGROUPS;
    public static final String RESOURCEGROUP_CHILDREN = RESOURCEGROUP + '/' + CHILDREN;
    public static final String RESOURCEGROUP_ROOTS = RESOURCEGROUPS + '/' + ROOTS;

    public static final String SKILLS = "skills";
    public static final String SKILL_ID_ATTRIBUTE = "skillId";
    public static final String SKILL = SKILLS + "/{" + SKILL_ID_ATTRIBUTE + '}';

    public static final String SKILLLEVELS = "skill-levels";
    public static final String SKILLLEVEL_ID_ATTRIBUTE = "skillLevelId";
    public static final String SKILLLEVEL = SKILLLEVELS + "/{" + SKILLLEVEL_ID_ATTRIBUTE + '}';

    public static final String QUALIFICATIONS = "qualifications";
    public static final String QUALIFICATION_ID_ATTRIBUTE = "qualificationId";
    public static final String QUALIFICATION = QUALIFICATIONS + "/{" + QUALIFICATION_ID_ATTRIBUTE + '}';

    public static final String ORGANIZATIONS = "organizations";
    public static final String ORGANIZATION_ID_ATTRIBUTE = "organizationId";
    public static final String ORGANIZATION = ORGANIZATIONS + "/{" + ORGANIZATION_ID_ATTRIBUTE + '}';

    public static final String CONTRACTS = "contracts";
    public static final String CONTRACT_ID_ATTRIBUTE = "contractId";
    public static final String CONTRACT = CONTRACTS + "/{" + CONTRACT_ID_ATTRIBUTE + '}';

    // Inventory
    public static final String SOFTWARES = "softwares";
    public static final String SOFTWARE_ID_ATTRIBUTE = "softwareId";
    public static final String SOFTWARE = SOFTWARES + "/{" + SOFTWARE_ID_ATTRIBUTE + '}';

    public static final String LICENSES = "licenses";
    public static final String LICENSE_ID_ATTRIBUTE = "licenseId";
    public static final String LICENSE = LICENSES + "/{" + LICENSE_ID_ATTRIBUTE + '}';

    public static final String COMPUTERS = "computers";
    public static final String COMPUTER_ID_ATTRIBUTE = "computerId";
    public static final String COMPUTER = COMPUTERS + "/{" + COMPUTER_ID_ATTRIBUTE + '}';

    public static final String DEVICES = "devices";
    public static final String DEVICE_ID_ATTRIBUTE = "deviceId";
    public static final String DEVICE = DEVICES + "/{" + DEVICE_ID_ATTRIBUTE + '}';

    public static final String SCREENS = "screens";
    public static final String SCREEN_ID_ATTRIBUTE = "screenId";
    public static final String SCREEN = SCREENS + "/{" + SCREEN_ID_ATTRIBUTE + '}';

    public static final String STUDIOMAPS = "studiomaps";
    public static final String STUDIOMAP_ID_ATTRIBUTE = "studiomapId";
    public static final String STUDIOMAP = STUDIOMAPS + "/{" + STUDIOMAP_ID_ATTRIBUTE + '}';

    public static final String ROOMS = "rooms";
    public static final String ROOM_ID_ATTRIBUTE = "roomId";
    public static final String ROOM = ROOMS + "/{" + ROOM_ID_ATTRIBUTE + '}';

    public static final String POOLS = "pools";
    public static final String POOL_ID_ATTRIBUTE = "poolId";
    public static final String POOL = POOLS + "/{" + POOL_ID_ATTRIBUTE + '}';

    public static final String MANUFACTURERS = "manufacturers";
    public static final String MANUFACTURER_ID_ATTRIBUTE = "manufacturerId";
    public static final String MANUFACTURER = MANUFACTURERS + "/{" + MANUFACTURER_ID_ATTRIBUTE + '}';

    public static final String PROCESSORS = "processors";
    public static final String PROCESSOR_ID_ATTRIBUTE = "processorId";
    public static final String PROCESSOR = PROCESSORS + "/{" + PROCESSOR_ID_ATTRIBUTE + '}';

    public static final String DEVICE_MODELS = "device-models";
    public static final String DEVICE_MODEL_ID_ATTRIBUTE = "devicemodelId";
    public static final String DEVICE_MODEL = DEVICE_MODELS + "/{" + DEVICE_MODEL_ID_ATTRIBUTE + '}';

    public static final String SCREEN_MODELS = "screen-models";
    public static final String SCREEN_MODEL_ID_ATTRIBUTE = "screenmodelId";
    public static final String SCREEN_MODEL = SCREEN_MODELS + "/{" + SCREEN_MODEL_ID_ATTRIBUTE + '}';

    public static final String COMPUTER_MODELS = "computer-models";
    public static final String COMPUTER_MODEL_ID_ATTRIBUTE = "computermodelId";
    public static final String COMPUTER_MODEL = COMPUTER_MODELS + "/{" + COMPUTER_MODEL_ID_ATTRIBUTE + '}';

    public static final String COMPUTER_TYPES = "computer-types";
    public static final String COMPUTER_TYPE_ID_ATTRIBUTE = "computertypeId";
    public static final String COMPUTER_TYPE = COMPUTER_TYPES + "/{" + COMPUTER_TYPE_ID_ATTRIBUTE + '}';

    public static final String DEVICE_TYPES = "device-types";
    public static final String DEVICE_TYPE_ID_ATTRIBUTE = "devicetypeId";
    public static final String DEVICE_TYPE = DEVICE_TYPES + "/{" + DEVICE_TYPE_ID_ATTRIBUTE + '}';

    public static final String LISTS_VALUES = "lists-values";
    public static final String LISTS_VALUES_ID_ATTRIBUTE = "listvaluesId";
    public static final String LIST_VALUES = LISTS_VALUES + "/{" + LISTS_VALUES_ID_ATTRIBUTE + '}';

    public static final String DYNAMIC_PATHS = "dynamic-paths";
    public static final String DYNAMIC_PATH_ID_ATTRIBUTE = "dynamicPathId";
    public static final String DYNAMIC_PATH = DYNAMIC_PATHS + "/{" + DYNAMIC_PATH_ID_ATTRIBUTE + '}';

    public static final String MOUNTPOINTS = "mount-points";
    public static final String MOUNTPOINT_ID_ATTRIBUTE = "mountPointId";
    public static final String MOUNTPOINT = MOUNTPOINTS + "/{" + MOUNTPOINT_ID_ATTRIBUTE + '}';

    public static final String PROXYTYPES = "proxy-types";
    public static final String PROXYTYPE_ID_ATTRIBUTE = "proxyTypeId";
    public static final String PROXYTYPE = PROXYTYPES + "/{" + PROXYTYPE_ID_ATTRIBUTE + '}';

    public static final String PROXIES = "proxies";
    public static final String PROXY_ID_ATTRIBUTE = "proxyId";
    public static final String PROXY = PROXIES + "/{" + PROXY_ID_ATTRIBUTE + '}';
    public static final String FILE_REVISION_PROXY_UPLOAD = FILEREVISION + '/' + PROXIES + '/' + UPLOAD;
    public static final String PROXY_DOWNLOAD = PROXY + '/' + DOWNLOAD;

    public static final String WORKOBJECTS = "workobjects";
    public static final String WORKOBJECT_ID_ATTRIBUTE = "workobjectId";
    public static final String BOUNDENTITY_ID_ATTRIBUTE = "boundentityId";
    public static final String WORKOBJECT = WORKOBJECTS + "/{" + WORKOBJECT_ID_ATTRIBUTE + "}/{"
            + BOUNDENTITY_ID_ATTRIBUTE + '}';

    public static final String LAST_PROXIES_WORKOBJECT_TASKTYPE = WORKOBJECT + "/" + TASKTYPE;

    public static final String ACLS = "acl";
    public static final String ACL_ID_ATTRIBUTE = "aclId";
    public static final String ACL = ACLS + "/{" + ACL_ID_ATTRIBUTE + '}';

    //
    public static final String WEEK_ACTIVITY_DURATION = "week-activity-duration";

    public static final String TASK_STATUS_NB_BY_TASKTYPES = "taskstatus-by-tasktypes";

    public static final String APPROVALNOTES_BY_SEQUENCES_IDS_BY_DATE = "approvalnotes-by-sequence-ids-by-date";
    public static final String APPROVALNOTES_BY_CATEGORIES_IDS_BY_DATE = "approvalnotes-by-category-ids-by-date";
    // F - Permission
    public static final String ROLES = "roles";
    public static final String ROLE_ID_ATTRIBUTE = "roleId";
    public static final String ROLE = ROLES + "/{" + ROLE_ID_ATTRIBUTE + '}';
    public static final String ROLESINGROUP = RESOURCEGROUPS + "/{" + RESOURCEGROUP_ID_ATTRIBUTE + "}/" + ROLES;
    public static final String ROLEINGROUP = ROLESINGROUP + "/{" + ROLE_ID_ATTRIBUTE + '}';

    public static final String PERMISSIONS = "permissions";
    public static final String BANS = "bans";
    public static final String USERACCOUNTS = "user-accounts";
    public static final String WHOIS = "whois";
    public static final String WHOIS_PERMISSIONS = WHOIS + '/' + PERMISSIONS;
    public static final String WHOIS_BANS = WHOIS + '/' + BANS;
    public final static String PRINCIPAL_ATTRIBUTE = "account_principal";
    public final static String SECRET_ATTRIBUTE = "account_secret";
    public final static String NEW_PASS_ATTRIBUTE = "new_password";
    public static final String USER_ACCOUNTS = USERACCOUNTS + "/{" + PRINCIPAL_ATTRIBUTE + '}';

    public static final String SECURITY_TEMPLATES = "security-templates";
    public static final String TEMPLATES = "templates";
    public static final String TEMPLATE_ID_ATTRIBUTE = "templateId";
    public static final String SECURITY_TEMPLATES_TEMPLATES = SECURITY_TEMPLATES + '/' + TEMPLATES;
    public static final String SECURITY_TEMPLATES_TEMPLATE = SECURITY_TEMPLATES_TEMPLATES + "/{"
            + TEMPLATE_ID_ATTRIBUTE + '}';
    public static final String ROLE_TEMPLATES = SECURITY_TEMPLATES + '/' + ROLES;
    public static final String ROLE_TEMPLATE = ROLE_TEMPLATES + "/{" + ROLE_ID_ATTRIBUTE + '}';

    public static final String APPLY_TEMPLATE = "apply-template";
    public static final String APPLY_SECURITY_TEMPLATE = SECURITY_TEMPLATES + '/' + APPLY_TEMPLATE + "/{"
            + TEMPLATE_ID_ATTRIBUTE + "}/{" + PROJECT_ID_ATTRIBUTE + '}';

    public static final String ALL_SERVICE_URLS = "services-urls";
    public static final int RESTRICT = 0;
    public static final int ALLOW_GET = 1;
    public static final int ALLOW_GETnPUT = 2;
    public static final int ALLOW_GETnPOST = 3;

    public static final String ENTITIES = "entities";
    public static final String ENTITY_ID_ATTRIBUTE = "entityId";
    public static final String ENTITY = ENTITIES + "/{" + ENTITY_ID_ATTRIBUTE + '}';
    public static final String ENUMS = "enums";

    public static final String COLLECTIONQUERIES = "collection-queries";
    public static final String ITEMSTRANSFORMERS = "item-transformers";
    public static final String FREE_QUERY = "freequery";
    public static final String IMAGES = "images";
    public static final String THUMBNAILS = "thumbnails";
    public static final String IMAGES_THUMBNAILS = IMAGES + '/' + THUMBNAILS;
    public static final String PREVIEWS = "previews";
    public static final String IMAGES_PREVIEWS = IMAGES + '/' + PREVIEWS;
    public static final String PING = "ping";
    public static final String DURATION = "duration";
    public static final String TASK_IDS = "taskIds";

    public static final String CREATE_FILE_TREE = "createFileTree";

    public static final String ANNOTATION_INDEXATION = "annotation-indexation";

}
