package fr.hd3d.common.client;

public interface Const
{
    public static final Integer INTERNALSTATUS_ENABLED = 0;
    public static final Integer INTERNALSTATUS_DISABLED = -1;
    // when an existing association between to instances is to be set to null
    public static final Long ID_RESET = -1L;
    // a valid id starts at 1L
    public static final Long ID_NULL = 0L;
    
    public static final String IDS = "ids";
    
    public static final String TRUE = "true";
    public static final String BULK = "bulk";
    public static final String MODE = "mode";
    public static final String BOUNDTASKS = "boundtasks";
    public static final String DYN = "dyn";
    public static final String TAGS = "tags";
    public static final String SYNC = "sync";
    public static final String EXTRAFIELDS = "extrafields";

    public static final String DATE = "date";
    public static final String STARTDATE = "startdate";
    public static final String ENDDATE = "enddate";

    public static final String PROJECTID = "projectid";
    public static final String TASKTYPE_IDS = "tasktypeids";
    public static final String ENTITYNAME = "entityname";
    
    
    public static final String CUSTOM = "custom";
    public static final String DYNMETADATA = "dynmetadata";
    public static final String METADATA_ITEM_TYPE = "metaData";
    public static final String FILEATTRIBUTE = "fileAttribute";
    public static final String APPROVALS = "approvals";// not a constraint but an allowed parameter in url
    public static final String ENTITYTASKLINKS = "entitytasklinks";// not a constraint but an allowed parameter in url

    public static final String JAVA_LANG_STRING = "java.lang.String";
    public static final String JAVA_LANG_BYTE = "java.lang.Byte";
    public static final String JAVA_LANG_LONG = "java.lang.Long";
    public static final String JAVA_LANG_INTEGER = "java.lang.Integer";
    public static final String JAVA_LANG_BOOLEAN = "java.lang.Boolean";
    public static final String JAVA_LANG_DOUBLE = "java.lang.Double";
    public static final String JAVA_LANG_FLOAT = "java.lang.Float";
    public static final String JAVA_LANG_ENUM = "java.lang.Enum";
    public static final String JAVA_UTIL_DATE = "java.util.Date";
    public static final String JAVA_UTIL_SET = "java.util.Set";
    public static final String JAVA_UTIL_LIST = "java.util.List";
    public static final String JAVA_SQL_TIMESTAMP = "java.sql.Timestamp";

    public static final String LIST_OF_APPROVALS = "java.util.List<fr.hd3d.model.persistence.IApprovalNote>";
    public static final String LIST_OF_PERSONS = "java.util.List<fr.hd3d.model.persistence.IPerson>";
    public static final String LIST_OF_PROJECTS = "java.util.List<fr.hd3d.model.persistence.IProject>";
    public static final String LIST_OF_RESOURCEGROUPS = "java.util.List<fr.hd3d.model.persistence.IResourceGroup>";
    public static final String LIST_OF_TASKS = "java.util.List<fr.hd3d.model.persistence.ITask>";

    public static final String BOUNDENTITY = "BoundEntity";
    public static final String STEP = "Step";

    public final static String CATEGORY_ROOT = "CONSTITUENT";
    public final static String SEQUENCE_ROOT = "SEQUENCE";
    public final static String RESOURCE_GROUP_ROOT = "ROOT";

    public final static String PLAYLIST_REVISION_GROUP_ROOT = "PLAYLIST";

    public final static String DYNMETADATATYPE_SIMPLE = "simple";
    public final static String DYNMETADATATYPE_LIST_DEFAULT_SEP = ";";
    public static final String TOTAL_ACTIVITIES_DURATION = "totalActivitiesDuration";
}
