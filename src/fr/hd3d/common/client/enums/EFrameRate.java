package fr.hd3d.common.client.enums;

public enum EFrameRate
{
    PAL_25, FILM_24, NTSC_30;
}
