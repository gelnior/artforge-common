package fr.hd3d.common.client.enums;

public enum EDynMetaDataOperator
{
    lt, gt, eq, geq, leq, neq, btw, in, notin, like, isnull, isnotnull;
}
