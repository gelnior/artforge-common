package fr.hd3d.common.client.enums;

/**
 * @author HD3D
 */
public enum EFileState
{
    WIP, PUBLISH, OLD;

    public static EFileState getDefault()
    {
        return WIP;
    }
}
