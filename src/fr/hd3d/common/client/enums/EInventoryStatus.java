package fr.hd3d.common.client.enums;

public enum EInventoryStatus
{
    UNKNOWN, OK, DESTROYED, OUT_OF_SERVICE, MISSING, AFTER_SALES;

    public static EInventoryStatus defaultStatus()
    {
        return OK;
    }

    public String statusColor()
    {
        if (this == OK)
        {
            return "#009900";
        }
        else if (this == DESTROYED)
        {
            return "#000000";
        }
        else if (this == OUT_OF_SERVICE)
        {
            return "#FF0000";
        }
        else if (this == MISSING)
        {
            return "#AAAAAA";
        }
        else if (this == AFTER_SALES)
        {
            return "#CC00FF";
        }
        return "#FFFFFF";
    }

    public String displayValue()
    {
        if (this == OK)
        {
            return "OK";
        }
        else if (this == DESTROYED)
        {
            return "Destroyed";
        }
        else if (this == OUT_OF_SERVICE)
        {
            return "Out of service";
        }
        else if (this == MISSING)
        {
            return "Missing";
        }
        else if (this == AFTER_SALES)
        {
            return "After sales";
        }
        return "Unknown";
    }

    public String displayColor()
    {
        if (this == DESTROYED)
        {
            return "white";
        }
        return "black";
    }
}
