package fr.hd3d.common.client.enums;

public enum EContractType
{
    CDI, CDD, INTERIM, FREELANCE, INTERMITTENT, UNKNOWN
}
