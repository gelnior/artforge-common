package fr.hd3d.common.client.enums;

public enum EConstraintLogicalOperator
{
    AND, OR, NOT;
}
