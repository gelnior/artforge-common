package fr.hd3d.common.client.enums;

/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public enum ESimpleActivityType
{
    MEETING, TECHNICAL_INCIDENT, OTHER;
}
