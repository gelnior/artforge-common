package fr.hd3d.common.client.enums;

public enum ESkillLevel
{
    VERY_LOW, LOW, AVERAGE, HIGH, VERY_HICH, UNKNOWN
}
