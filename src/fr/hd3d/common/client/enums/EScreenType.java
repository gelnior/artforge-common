package fr.hd3d.common.client.enums;

public enum EScreenType
{
    LCD, CRT, UNKNOWN
}
