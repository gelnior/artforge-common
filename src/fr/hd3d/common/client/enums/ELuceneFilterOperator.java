package fr.hd3d.common.client.enums;

public enum ELuceneFilterOperator
{
    eq, neq, in, notin, prefix;
}
