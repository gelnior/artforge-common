package fr.hd3d.common.client.enums;

public enum ESkillMotivation
{
    VERY_LOW, LOW, AVERAGE, HIGH, VERY_HIGH, UNKNOWN
}
