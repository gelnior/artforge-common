package fr.hd3d.common.client.enums;

public enum EScreenQualification
{
    REFERENCE, COLOR, GRAPHIC, OFFICE, OUT, UNKNOWN
}
