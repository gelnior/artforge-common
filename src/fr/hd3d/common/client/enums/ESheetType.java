package fr.hd3d.common.client.enums;

public enum ESheetType
{
    BREAKDOWN, PRODUCTION, TASK, TEAM, PROJECT, MACHINE;
}
