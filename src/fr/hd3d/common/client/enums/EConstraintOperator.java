package fr.hd3d.common.client.enums;

public enum EConstraintOperator
{
    lt, gt, eq, geq, leq, neq, btw, in, notin, like, isnull, isnotnull, inSequence, inSequenceShots, hasTag, hasNotTag, prefix;
}
