package fr.hd3d.common.client.enums;

/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public enum ETaskStatus
{
    /* same as EApprovaNoteStatus. */
    STAND_BY, WORK_IN_PROGRESS, WAIT_APP, OK, CANCELLED, CLOSE, TO_DO, RETAKE, NEW;

    public static ETaskStatus defaultStatus()
    {
        return NEW;
    }
}
