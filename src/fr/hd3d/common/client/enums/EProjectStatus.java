package fr.hd3d.common.client.enums;

/**
 * Project status
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public enum EProjectStatus
{
    OPEN, CLOSED, WAITING;
}
