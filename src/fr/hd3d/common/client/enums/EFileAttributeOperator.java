package fr.hd3d.common.client.enums;

public enum EFileAttributeOperator
{
    eq, neq, like, isnull, isnotnull;
}
