package fr.hd3d.common.client.enums;

public enum ELuceneFilterLogicalOperator
{
    AND, OR, NOT;
}
