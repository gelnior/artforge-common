package fr.hd3d.common.client.enums;

/**
 * @author Try LAM
 */
public enum EFileStatus
{
    LOCKED, UNLOCKED, MOVED, DELETED, COMPRESSED;
}
