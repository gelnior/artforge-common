package fr.hd3d.common.client.enums;

public enum EItemFilterLogicalOperator
{
    AND, OR, NOT;
}
