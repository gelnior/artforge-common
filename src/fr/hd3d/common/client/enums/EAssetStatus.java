package fr.hd3d.common.client.enums;

/**
 * @author Try LAM
 */
public enum EAssetStatus
{
    VALIDATED, UNVALIDATED, LOCKED, UNLOCKED, MOVED, DELETED, COMPRESSED;
}
