package fr.hd3d.common.client.enums;

public enum EApprovalNoteStatus
{
    /* same as ETaskStatus. */
    STAND_BY, WORK_IN_PROGRESS, WAIT_APP, OK, CANCELLED, CLOSE, TO_DO, RETAKE, NEW;

    public static EApprovalNoteStatus defaultStatus()
    {
        return NEW;
    }
}
