package fr.hd3d.common.client.enums;

/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public enum EWorkerStatus
{
    NO_WORKER, INACTIVE, ACTIVE, RUNNING;
}
