package fr.hd3d.common.client.enums;

public enum ELicenseType
{
    CLASSIC, NODE_LOCK, DOMAIN, RENDER, FLOAT
}
