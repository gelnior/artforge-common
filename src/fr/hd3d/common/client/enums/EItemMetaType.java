package fr.hd3d.common.client.enums;

/**
 * Sheet's ItemGroup's Item's Metatype
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public enum EItemMetaType
{
    LONG_TEXT, SHORT_TEXT, ENUMERATION, BOOLEAN, NUMBER;
}
