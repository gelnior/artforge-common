package fr.hd3d.common.client;

public interface Persist
{

    final public static String PREPERSIST = "prePersist";
    final public static String POSTPERSIST = "postPersist";
    final public static String PREUPDATE = "preUpdate";
    final public static String POSTUPDATE = "postUpdate";
    final public static String PREDELETE = "preDelete";
    final public static String POSTDELETE = "postDelete";

}
