package fr.hd3d.common.client;

public interface DateFormat
{
    public static final String DATE_STRING = "dd-MM-yyyy";

    public static final String FRENCH_DATE_STRING = "dd/MM/yyyy";

    /** Date format used by web services for parsing sent data and for JSON generation. */
    public static final String DATE_TIME_STRING = "yyyy-MM-dd HH:mm:ss";

    public static final String DATE_TIME_ISO_8607_STRING = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

    public static final String FRENCH_DATE_TIME_STRING = "dd/MM/yyyy HH:mm:ss";

    public static final String REVERSED_FRENCH_STRING = "yyyy/MM/dd";

    public static final String TIME_STRING = "HH:mm";

    public static final String DATESTAMP_STRING = "yyyy-MM-dd";

    public static final String DATE_AND_DAY_STRING = "yyyy-MM-dd (EEE)";

    public static String TIMESTAMP_FORMAT_STRING = "yyyy-MM-dd HH:mm:ss.S";

    public static String AUDIT_PURGE_FILE_TIMESTAMP_FORMAT = "yyyy_MM_dd-HH_mm_ss";

    public static String LUCENE_VERSION_DATE_FORMAT = "yyyyMMddHHmmss";
}
