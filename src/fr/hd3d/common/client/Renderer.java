package fr.hd3d.common.client;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Renderer
{
    public static final String DEFAULT = "default";
    public static final String BOOLEAN = "boolean";
    public static final String SHORT_TEXT = "short-text";
    public static final String LONG_TEXT = "long-text";
    public static final String BIG_TEXT = "big-text";
    public static final String INT = "int";
    public static final String TASK_TYPE = "task-type";
    public static final String BOUND_ENTITY = "bound-entity";
    public static final String WORK_OBJECT = "work-object";
    public static final String APPROVAL = "approval";
    public static final String APPROVAL_COMPACT = "approval-compact";
    public static final String RATING = "rating";
    public static final String DURATION = "duration";
    public static final Object DURATION_ACTIVITY = "activity-duration";
    public static final String DATE_AND_DAY = "date-and-day";
    public static final String THUMBNAIL = "thumbnail";
    public static final String TASK_STATUS = "task-status";
    public static final String PERSON_TASK = "person-task";
    public static final String BOLD = "bold";
    public static final String DATE = "date";
    public static final String FLOAT = "float";
    public static final String HYPERLINK = "hyperlink";
    public static final String POURCENT = "pourcent";
    public static final String ACTIVITIES_DURATION = "activitiesDurationRenderer";
    public static final String SHORT_DATE = "short-date";
    public static final String FRENCH_DATE = "french-date";
    public static final String REVERSED_FRENCH_DATE = "reversed-french-date";
    public static final String HOURS_DURATION = "hours-duration";
    public static final String HOURS = "hours";
    public static final String ACTITVITY_DURATION = "activity-duration";
    public final static String NB_OK_TASKS = "nb-ok-tasks";
    public static final String LAST_PUBLISHED = "last-published";
    public static final String PROJECT = "Project";
    public static final String PROJECT_STATUS = "project-status";
    public static final String PROJECT_TYPE = "project-type";
    public static final String GROUP = "Group";
    public static final String RESOURCE_GROUP = "ResourceGroup";
    public static final String TAG = "Tag";
    public static final String PERSON = "Person";
    public static final String CATEGORY = "Category";
    public static final String SEQUENCE = "Sequence";
    public static final String TASK = "Task";
    public static final Object WORK_OBJECT_BIG = "work-object-big";
    public static final String CONTRACT_TYPE = "contract-type";

    public static final String SCREEN_TYPE = "screen-type";
    public static final String SCREEN_QUALIFICATION = "screen-qualification";
    public static final String INVENTORY_STATUS = "inventory-status";
    public static final String WORKER_STATUS = "worker-status";
    public static final String LICENSE_TYPE = "license-type";
    public static final String RECORD = "record";
    public static final String FREQUENCY = "frequency";
    public static final String SCREEN_SIZE = "screen-size";
    public static final String RAM = "ram";
    public static final String STEP = "step";
    public static final Object PERSON_LOGIN = "person-login";

    /**
     * Key=data type; value=list of renderers, the first one is the default. Note: only use string as key for the map,
     * since it is used by gxt.
     */
    public static final Map<String, List<String>> rendererMaps = new HashMap<String, List<String>>();

    static
    {
        rendererMaps.put(Const.JAVA_LANG_STRING, Arrays.asList(DEFAULT, SHORT_TEXT, LONG_TEXT, THUMBNAIL, HYPERLINK,
                BOLD, NB_OK_TASKS));

        rendererMaps.put(Const.JAVA_LANG_BOOLEAN, Arrays.asList(BOOLEAN));

        rendererMaps.put(Entity.TASKTYPE, Arrays.asList(TASK_TYPE));

        rendererMaps.put(Const.BOUNDENTITY, Arrays.asList(BOUND_ENTITY));

        rendererMaps.put(Entity.APPROVALNOTE, Arrays.asList(APPROVAL, APPROVAL_COMPACT));

        rendererMaps.put(Const.LIST_OF_APPROVALS, Arrays.asList(APPROVAL, APPROVAL_COMPACT));

        rendererMaps.put(Const.JAVA_LANG_LONG, Arrays.asList(RATING, HOURS, DURATION, ACTIVITIES_DURATION));

        rendererMaps.put(Const.JAVA_LANG_DOUBLE, Arrays.asList(RATING, HOURS, DURATION, ACTIVITIES_DURATION));

        rendererMaps.put(Const.JAVA_UTIL_DATE, Arrays.asList(DATE, DATE_AND_DAY, SHORT_DATE, FRENCH_DATE,
                REVERSED_FRENCH_DATE, LAST_PUBLISHED));

        rendererMaps.put(Const.JAVA_SQL_TIMESTAMP, Arrays.asList(DATE, DATE_AND_DAY, SHORT_DATE, FRENCH_DATE,
                REVERSED_FRENCH_DATE, LAST_PUBLISHED));

        rendererMaps.put(Entity.TASKSTATUS, Arrays.asList(TASK_STATUS));

        rendererMaps.put(Entity.PERSON, Arrays.asList(PERSON_TASK));

        rendererMaps.put(Const.JAVA_LANG_FLOAT, Arrays.asList(FLOAT));

        /* TODO hashmap with several "" key value !!!! */
        rendererMaps.put("", Arrays.asList(POURCENT));

        rendererMaps.put(Const.JAVA_LANG_INTEGER, Arrays.asList(INT, HOURS_DURATION, ACTITVITY_DURATION));

        rendererMaps.put(Const.JAVA_LANG_BYTE, Arrays.asList(INT, HOURS_DURATION, ACTITVITY_DURATION));

        rendererMaps.put(Entity.PROJECT, Arrays.asList(PROJECT));

        rendererMaps.put(Const.LIST_OF_PROJECTS, Arrays.asList(PROJECT));

        /* TODO hashmap with several "" key value !!!! */
        rendererMaps.put("", Arrays.asList(PROJECT_STATUS));

        rendererMaps.put(Entity.PROJECTTYPE, Arrays.asList(PROJECT_TYPE));

        /* TODO hashmap with several "" key value !!!! */
        rendererMaps.put("", Arrays.asList(GROUP));

        /* TODO hashmap with several "" key value !!!! */
        rendererMaps.put("", Arrays.asList(TAG));

        rendererMaps.put(Const.LIST_OF_PERSONS, Arrays.asList(PERSON));

        rendererMaps.put(Const.LIST_OF_TASKS, Arrays.asList(TASK));

        rendererMaps.put(Entity.CATEGORY, Arrays.asList(CATEGORY));

        rendererMaps.put(Entity.SEQUENCE, Arrays.asList(SEQUENCE));

        /* TODO hashmap with several "" key value !!!! */
        rendererMaps.put("", Arrays.asList(CONTRACT_TYPE));

    }
}
